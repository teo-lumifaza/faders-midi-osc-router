**Warning: the source code is imperfect! See the [blog post](https://info.lumifaza.org/2022/11/Imperfect-source-codes-faders/) for background.**

# Hackable & extensible MIDI & OSC router
* written in Python
* can synchronize state across multiple devices like digital audio mixers, MIDI control surfaces, electronic musical instruments
* built-in MIDI & OSC transceivers
* built-in web-based ON AIR sign
* routing is done by specifying a Hub connecting multiple Endpoints (transceivers)
* new types of state transmitters and receivers (e.g. GPIO) can be specified by writing a class with 2 methods

Tested with:
* OSC:
  * Behringer X32 Rack
  * Behringer XR18
* MIDI:
  * Behringer BCF2000
  * Behringer BCR2000
  * iCon iControls Mini

## Dependencies
`apt install python3-websockets python3-liblo`

(if you don't run Debian/Ubuntu, `pip3 install websockets pyliblo3` should also work, but wasn't tested)

It also depends on `mididings` which is not packaged in PyPi. See [updated fork repository & README here](https://github.com/noedigcode/mididings) for code, build & install instructions.

## Running
The entry point is `./ctl.py`

## To do
* Make it a proper library/framework.
* Port it to some compiled language without garbage collection (Rust?). Python is not real-time-safe so it may introduce random latencies. However, it works well enough for controlling audio mixer from MIDI controllers.
