#!/usr/bin/python3

'''
 * Copyright 2022-2023 Teodor Wozniak
 *
 * USE, DISTRIBUTION AND REPRODUCTION OF THIS SOFTWARE IS
 * GOVERNED BY A BSD-STYLE SOURCE LICENSE INCLUDED WITH THIS SOURCE
 * IN 'COPYING'. PLEASE READ THESE TERMS BEFORE USING AND DISTRIBUTING.
'''

import mididings
import mididings.event
import mididings.engine
from mididings import *

import liblo
import datetime
import threading
import time
import sys, traceback, signal
import requests

import websocket_onair
#from pyudmx import pyudmx

# this is for X32 (probably also M32)
# change port to 10024 for X Air series
#osc_mixer = liblo.Address('osc.udp://10.0.0.111:10023/')
osc_mixer = liblo.Address('osc.udp://192.168.1.117:10023/')

VALUE_LIFETIME = datetime.timedelta(seconds=2)

all_hubs = []
osc_address_to_endpoint = {}
exit_event = threading.Event()

config(
    backend='alsa',
    client_name='x32rack-ctl',
    in_ports=[('ctlin', '.*BCF2000 MIDI 1')],
    out_ports=[('ctlout', '.*BCF2000 MIDI 1')],
    # you can add connect more input/output MIDI devices - by specifying more tuples within lists above
    # remember to set the MIDI channels/controls on the control surfaces to avoid overlapping in such case
)

class EndpointNotReadyException(Exception):
    pass

class Endpoint:
    def __init__(self):
        self.precision = 0.0065 # feedback prevention threshold
        self.send_unchanged = False
    def prec(self, newprec):
        self.precision = newprec
        return self

class MIDICtrlEndpoint(Endpoint):
    def __init__(self, ctrl, channel=1, scale=127.0):
        Endpoint.__init__(self)
        self.channel = channel
        self.ctrl = ctrl
        self.scale = scale
    def __str__(self):
        return 'MIDICtrlEndpoint(ctrl=%i, channel=%i)' % (self.ctrl, self.channel)
    def getMididingsPatch(self):
        return Filter(CTRL) >> ChannelFilter(self.channel) >> CtrlFilter(self.ctrl)
    def setRecvCallback(self, func):
        # this sets callback that Hub will use to receive value from the Endpoint
        global mainpatch
        mainpatch.append(self.getMididingsPatch() >> Call(lambda ev: func(float(ev.value)/self.scale)))
    def sendValue(self, value):
        # this is called by the Hub to send the value to the endpoint (in this case, a MIDI control surface)
        if not mididings.engine.active():
            raise EndpointNotReadyException()
        for p in mididings.engine.out_ports():
            midimsg = mididings.event.CtrlEvent(port=p, channel=self.channel, ctrl=self.ctrl, value=int(value*self.scale))
            mididings.engine.output_event(midimsg)

class OSCEndpoint(Endpoint):
    def __init__(self, path):
        global osc_address_to_endpoint
        Endpoint.__init__(self)
        self.path = path
        osc_address_to_endpoint[path] = self
        
    def __str__(self):
        return self.path
    def query(self):
        # this is called by the Hub to query current value from the device (in this case, a digital mixing console)
        srv.send(osc_mixer, self.path)
    def setRecvCallback(self, func):
        def cb(path, args):
            f, = args
            func(f)
        srv.add_method(self.path, self.osc_type, cb)
    def deleteRecvCallback(self):
        srv.del_method(self.path, self.osc_type)


class OSCFloatEndpoint(OSCEndpoint):
    def __init__(self, path, scale=1.0):
        OSCEndpoint.__init__(self, path)
        self.scale = scale
        self.osc_type = 'f'
        
    def sendValue(self, value):
        srv.send(osc_mixer, self.path, value*self.scale)

class OSCIntEndpoint(OSCEndpoint):
    def __init__(self, path, invert01=False):
        OSCEndpoint.__init__(self, path)
        self.osc_type = 'i'
        self.invert01 = invert01
    
    def sendValue(self, value):
        srv.send(osc_mixer, self.path, (1-int(value)) if self.invert01 else int(value))
        
    def setRecvCallback(self, func):
        def cb(path, args):
            value, = args
            func((1-int(value)) if self.invert01 else int(value))
        srv.add_method(self.path, self.osc_type, cb)

class NrpnHandler:
    # this is an intermediate entity used for transceiving NRPN MIDI messages
    # they're useful for transferring values with 14-bit precision
    # instead of the MIDI's usual 7-bit.
    # it was tested with Behringer BCF2000.
    def __init__(self, channel=1):
        self.reset()
        self.handlers = dict()
        self.channel = channel
        self._register()
    
    def reset(self):
        self.nrpn_h = None
        self.nrpn_l = None
        self.data_h = None
        self.data_l = None
    
    def setRecvCallback(self, nrpn, cb):
        self.handlers[nrpn] = cb
    
    def handleEvent(self, ev):
        if ev.ctrl==99:
            self.nrpn_h = ev.value
        elif ev.ctrl==98:
            self.nrpn_l = ev.value
        elif ev.ctrl==6:
            self.data_h = ev.value
        elif ev.ctrl==38:
            self.data_l = ev.value
            self.gotmsg()
    
    def gotmsg(self):
        if self.nrpn_h==None or self.nrpn_l==None or self.data_h==None or self.data_l==None:
            return
        nrpn = self.nrpn_l + (self.nrpn_h << 7)
        value = self.data_l + (self.data_h << 7)
        print("got NRPN %i = %i " % (nrpn, value))
        if nrpn in self.handlers:
            self.handlers[nrpn](value/16383.0)
    
    def _sendCtrls(self, *pairs):
        for p in mididings.engine.out_ports():
            for ctrl, value in pairs:
                mididings.engine.output_event(mididings.event.CtrlEvent(port=p, channel=self.channel, ctrl=ctrl, value=value))
    def send(self, nrpn, value):
        if not mididings.engine.active():
            raise EndpointNotReadyException()
        intval = int(value*16383.0)
        self._sendCtrls( (99, nrpn >> 7), (98, nrpn & 0x7f), (6, intval >> 7), (38, intval & 0x7f) )
    
    def getMididingsPatch(self):
        return Filter(CTRL) >> ChannelFilter(self.channel)
    def _register(self):
        global mainpatch
        mainpatch.append(self.getMididingsPatch() >> Call(self.handleEvent))

class NrpnEndpoint(Endpoint):
    def __init__(self, handler, nrpn):
        Endpoint.__init__(self)
        self.handler = handler
        self.nrpn = nrpn
    
    def setRecvCallback(self, func):
        self.handler.setRecvCallback(self.nrpn, func)
    
    def sendValue(self, value):
        self.handler.send(self.nrpn, value)

class SendOnlyEndpoint(Endpoint):
    # a write-only endpoint - no value change will ever originate from it - a passive listener
    def setRecvCallback(self, func):
        pass



import socketserver
off_air_color = "green"
off_air_blink = False

class OffAirColorUDPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        global off_air_color, off_air_blink
        data = self.request[0].decode("ascii").strip()
        socket = self.request[1]
        if data == "yellow":
            if off_air_color == "yellow":
                off_air_blink = not off_air_blink
        else:
            off_air_blink = False
        off_air_color = data

def companion_color(s):
    return # disabled
    requests.post("http://127.0.0.1:8888/api/custom-variable/air_color/value?value="+s)

udmx_device = None

def led_color(r, g, b):
    return # disabled
    global udmx_device
    try:
        if udmx_device is None:
            udmx_device = pyudmx.uDMXDevice()
            udmx_device.open()
        udmx_device.send_multi_value(1, [r, g, b, 255])
    except Exception as e:
        try:
            udmx_device.close()
        except:
            pass
        udmx_device = None
        print("udmx send failed: " + str(e))

class StudioOnAir:
    def __init__(self):
        self.channels = []
        self.blink_start_time = None

    class EventEndpoint(SendOnlyEndpoint):
        def __init__(self, onair):
            Endpoint.__init__(self)
            self.send_unchanged = True
            self.onair = onair

        def sendValue(self, value):
            # ignore the value - we'll read it from Hub
            self.onair.refresh()

    class OnAirChannel:
        def __init__(self, onair):
            self.onair = onair
            self.fader_eps = []
            self.mute_eps = []

        def addFaderEndpoint(self):
            r = StudioOnAir.EventEndpoint(self.onair)
            self.fader_eps.append(r)
            return r

        def addMuteEndpoint(self):
            r = StudioOnAir.EventEndpoint(self.onair)
            self.mute_eps.append(r)
            return r

        def getState(self):
            for mute in self.mute_eps:
                try:
                    if mute.hub.expired:
                        print("Value from mute expired")
                        return None
                    if mute.hub.value > 0:
                        return False
                except:
                    print("Hub not yet set?! (mute)")

            for fader in self.fader_eps:
                try:
                    if fader.hub.expired:
                        print("Value from fader expired")
                        return None
                    if fader.hub.value > 0:
                        return True
                except:
                    print("Hub not yet set?! (fader)")

            return False

    def addChannel(self):
        r = self.OnAirChannel(self)
        self.channels.append(r)
        return r

    def refresh(self):
        for ch in self.channels:
            state = ch.getState()
            if state is None:
                websocket_onair.out("unknown")
                led_color(0, 0, 128)
                companion_color("?")
                return
            if state:
                global off_air_blink
                off_air_blink = False
                websocket_onair.out("onair")
                led_color(255, 0, 0)
                companion_color("red")
                return

        websocket_onair.out("offair")
        if off_air_color=="green":
            led_color(0, 56, 0)
            companion_color("green")
        elif off_air_color=="yellow":
            if off_air_blink:
                if self.blink_start_time is None:
                    self.blink_start_time = time.monotonic()
            else:
                self.blink_start_time = None
            if (not off_air_blink) or ( (time.monotonic()-self.blink_start_time)%1 < 0.5):
                led_color(255, 255, 0)
                companion_color("yellowon" if off_air_blink else "yellow")
            else:
                led_color(0, 0, 0)
                companion_color("black")
                companion_color("yellowoff")

class Hub:
    # the Hub works like an Ethernet hub - it broadcasts value change to all connected Endpoints
    # except the one that it originated from.
    def __init__(self, *endpoints):
        self.endpoints = []
        self.value = None
        self.recently_sent = []
        self.last_change_from = None
        self.failed = dict()
        for ep in endpoints:
            self.addEndpoint(ep, False)
        self.queryAll()
        all_hubs.append(self)
        self.expire_time = datetime.datetime(2000, 6, 6)
        
    @staticmethod
    def queryEndpoint(ep):
        if hasattr(ep, 'query'):
            ep.query()
    def queryAll(self):
        for ep in self.endpoints:
            self.queryEndpoint(ep)
    def addEndpoint(self, ep, query=True):
        ep.hub = self
        self.endpoints.append(ep)
        def cb(v, ep=ep):
            same = (v == self.value) or ((self.last_change_from != ep) and (self.value is not None) and (abs(v-self.value)<ep.precision))
            changed = not same
            self.last_change_from = ep
            self.setValue(v)
            for rs in self.recently_sent:
                sec = (datetime.datetime.now()-rs[1]).total_seconds()
                if (rs[0] == ep) and (sec < 0.1) and (abs(rs[2]-v)<ep.precision):
                    print("Bounced signal got from %s in %fms, diff=%f" % (str(ep), sec*1000, rs[2]-v))
                    return
            print("Received %f from %s" % (v, str(ep)))
            self.sendToAllExcept(ep, changed)
        ep.setRecvCallback(cb)
        if query:
            self.queryEndpoint(ep)
    def deleteEndpoint(self, ep):
        if ep in self.endpoints:
            ep.deleteRecvCallback()
            self.endpoints.remove(ep)
    
    def sendToAllExcept(self, ignoredep, changed):
        if self.value==None:
            return
        try:
            for ep in self.endpoints:
                if (ignoredep != ep) and (changed or ep.send_unchanged):
                    try:
                        ep.sendValue(self.value)
                        self.recently_sent.append((ep, datetime.datetime.now(), self.value))
                        self.failed.pop(ep, None) # mark as not failed
                        print("Sent %f to %s" % (self.value, str(ep)))
                    except Exception as e:
                        self.failed[ep] = self.value
                        print("Sending %f to %s failed" % (self.value, str(ep)))
                        print(str(e))
                        traceback.print_exc()
        except Exception as e:
            print(str(e))
            traceback.print_exc()
    def sendValue(self, value):
        changed = value != self.value
        self.setValue(value)
        self.sendToAllExcept(None, changed)
    
    def setValue(self, value):
        self.value = value
        self.expire_time = datetime.datetime.now() + VALUE_LIFETIME

    @property
    def expired(self):
        return datetime.datetime.now() > self.expire_time

    def __str__(self):
        return "Hub(" + ", ".join(map(str, self.endpoints)) + ")"


srv = liblo.ServerThread(19001)

mainpatch = [
    Print(),
]

# uncomment to enable bottom rows main mix level regulation (BCR2000)
#for x in range(1,17):
#    Hub(OSCFloatEndpoint('/ch/%02d/mix/fader' % x), MIDICtrlEndpoint(ctrl=88+x))

#Hub(OSCFloatEndpoint('/ch/07/mix/fader'), MIDICtrlEndpoint(ctrl=99))
#Hub(OSCFloatEndpoint('/config/mute/1'), MIDICtrlEndpoint(ctrl=3, channel=2))
#Hub(OSCFloatEndpoint('/fx/3/par/10').prec(0.021), MIDICtrlEndpoint(ctrl=1))
#Hub(OSCFloatEndpoint('/fx/3/par/11').prec(0.021), MIDICtrlEndpoint(ctrl=2))
#Hub(OSCFloatEndpoint('/rtn/aux/mix/fader'), MIDICtrlEndpoint(ctrl=87))

strips = [
    "/ch/01", # 
    "/ch/02", # 
    "/ch/03", # 
    "/ch/04", # 
    "/ch/05", # 
    "/ch/06", # 
    "/auxin/01",  # AuxIn1-2 (they are linked in the console)
    "/main/st" # Master LR
]
ch_to_solo_id = {
    "/main/st": 71
}
for index in range(0, 32):
    ch_to_solo_id["/ch/%02d" % (index+1)] = index+1
for index in range(0, 8):
    ch_to_solo_id["/auxin/%02d" % (index+1)] = index+33

nrpn = NrpnHandler(channel=2)

#Hub(OSCFloatEndpoint("/config/solo/level"), MIDICtrlEndpoint(ctrl=8, channel=2))

onair = StudioOnAir()
onair_strips = []
for index, strip in enumerate(strips, 1):
    fader_args = ()
    mute_args = ()
    if index <= 4: # these channels are microphone channels that trigger the ON AIR sign
        oach = onair.addChannel()
        fader_args = oach.addFaderEndpoint(),
        mute_args = oach.addMuteEndpoint(),
        onair_strips.append(strip)
        
    Hub(OSCFloatEndpoint(strip + '/mix/fader'), NrpnEndpoint(nrpn, index), *fader_args)
    Hub(OSCIntEndpoint(strip + '/mix/on', invert01=True), MIDICtrlEndpoint(ctrl=16+index, channel=2), *mute_args)
    # TODO we can add here mute groups and DCAs and subgroups for ON AIR integration ... but it requires knowing signal flow, complicated!
    # you can call oach.add*Endpoint for all channel strips that affect whether the microphone is ON the AIR (e.g. DCAs, mute group, subgroups, matrices) - the sign will light only when all the added faders are up and unmuted
    Hub(OSCIntEndpoint('/-stat/solosw/%02d' % ch_to_solo_id[strip]), MIDICtrlEndpoint(ctrl=8+index, channel=2))

def subscribeloop():
    i = 0
    freq = 20
    while True:
        try:
            if i % (freq*7) == 0:
                srv.send(osc_mixer, '/xremote')
            # if you have only few values to monitor, the following may work better:
            #srv.send(osc_mixer, '/subscribe', '/ch/01/mix/fader')
            #srv.send(osc_mixer, '/subscribe', '/ch/02/mix/fader')

            #srv.send(osc_mixer, '/subscribe', '/ch/01/mix/on')
            #srv.send(osc_mixer, '/subscribe', '/ch/02/mix/on')
            strip = onair_strips[i%len(onair_strips)] 
            srv.send(osc_mixer, strip + '/mix/fader')
            srv.send(osc_mixer, strip + '/mix/on')
        except Exception as e:
            print(str(e))
        if exit_event.wait(1.0/freq):
            break
        i += 1

def send_deferred_loop():
    # if we have a detectable error, we'll retry sending value update
    # (remember that OSC is UDP so most network errors won't be detected!)
    while True:
        try:
            dtnow = datetime.datetime.now()
            total_before = 0
            total_after = 0
            for h in all_hubs:
                # try to send failed events:
                for ep in list(h.failed.keys()):
                    try:
                        value = h.failed[ep]
                        ep.sendValue(value)
                        print("Successfully resent failed event for %s = %f" % (str(ep), value))
                        del h.failed[ep]
                    except:
                        print("Failed event for %s = %f failed again" % (str(ep), value))

                # garbage collection,
                # remove old events from hubs
                total_before += len(h.recently_sent)
                def fltr(rs):
                    if (dtnow-rs[1]).total_seconds()>0.1:
                        return False
                    return True
                h.recently_sent = list(filter(fltr, h.recently_sent))
                total_after += len(h.recently_sent)
            if total_before>0:
                print("GC: before %d, after %d" % (total_before, total_after))
        except Exception as e:
            print(str(e))
            traceback.print_exc()
        if exit_event.wait(1):
            break

def exit_handler(signal, frame):
    print("Exit requested...")
    exit_event.set()
    mididings.engine.quit()
    # TODO stop event loop within websocket_onair.py
    #sys.exit(0)

signal.signal(signal.SIGINT, exit_handler)
signal.signal(signal.SIGTERM, exit_handler)

for hub in all_hubs:
    print(str(hub))

def oscfallback(path, args):
    pass
    #print("received unknown message '%s'" % path)

srv.add_method(None, None, oscfallback)

def off_air_color_thread():
    with socketserver.UDPServer(("127.0.0.1", 10333), OffAirColorUDPHandler) as server:
        server.serve_forever()


def thr_starter():
    while not mididings.engine.active():
        time.sleep(0.1)
    srv.start()
    threading.Thread(target = subscribeloop).start()
    threading.Thread(target = send_deferred_loop).start()
    websocket_onair.start_async_thread()
    threading.Thread(target = off_air_color_thread).start()
    print("Spawned all threads.")

threading.Thread(target = thr_starter).start()

print("Starting MIDI event loop.")

run(
    scenes = {
        1: Scene("Default", []),
    },
    pre = mainpatch
)

print("MIDI event loop finished.")
