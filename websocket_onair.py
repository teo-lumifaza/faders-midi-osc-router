'''
 * Copyright 2022 Teodor Wozniak
 *
 * USE, DISTRIBUTION AND REPRODUCTION OF THIS SOFTWARE IS
 * GOVERNED BY A BSD-STYLE SOURCE LICENSE INCLUDED WITH THIS SOURCE
 * IN 'COPYING'. PLEASE READ THESE TERMS BEFORE USING AND DISTRIBUTING.
'''

import asyncio, websockets, logging, threading, datetime

LIFETIME = datetime.timedelta(seconds=2)

class WebSocketOutput:
    def __init__(self):
        self.websockets = set()
        self.curr_line = None
        self.last_update = datetime.datetime(2000, 6, 6)
    
    async def run(self, port=9998):
        asyncio.ensure_future(self.resend_loop())
        await websockets.serve(self.websocket_handler, "0.0.0.0", port)
    
    def log_client(self, what):
        logging.info("client " + what + ", current clients count " + str(len(self.websockets)))
    
    async def websocket_handler(self, ws, path):
        if self.curr_line is not None:
            try:
                await ws.send(self.curr_line)
            except Exception as e:
                logging.exception("initial send failed, closing connection")
                return
        
        self.websockets.add(ws)
        self.log_client(ws.remote_address[0] + " connected")

        try:
            await ws.recv() # normally client won't send anything, so close connection when they do
        except websockets.exceptions.ConnectionClosed:
            pass
        except Exception as e:
            logging.exception("recv failed, connection closed?")
        
        self.websockets.remove(ws)
        self.log_client(ws.remote_address[0] + " disconnected")
    
    @staticmethod
    async def send(ws, line):
        if line is None:
            return
        try:
            await ws.send(line)
        except websockets.exceptions.ConnectionClosed:
            pass
        except Exception as e:
            logging.exception("websocket send failed")
            
    def out(self, line):
        self.last_update = datetime.datetime.now()
        if line == self.curr_line:
            return
        self.curr_line = line
        for ws in self.websockets:
            asyncio.ensure_future(self.send(ws, line))

    async def resend_loop(self):
        while True:
            if datetime.datetime.now() > self.last_update + LIFETIME:
                self.curr_line = 'unknown'
            for ws in self.websockets:
                asyncio.ensure_future(self.send(ws, self.curr_line))
            await asyncio.sleep(0.8)

    def out_from_different_thread(self, line, loop):
        async def out_wrapper():
            self.out(line)
        asyncio.run_coroutine_threadsafe(out_wrapper(), loop)
        #self.curr_line = line
        #for ws in self.websockets:
        #    asyncio.run_coroutine_threadsafe(self.send(ws, line), loop)


wsout = WebSocketOutput()
loop = None

def start_async_thread():
    def thrfunc():
        global loop
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        asyncio.ensure_future(wsout.run())
        loop.run_forever()

    threading.Thread(target=thrfunc).start()

def out(line):
    wsout.out_from_different_thread(line, loop)
